#include "alphabet.h"

byte POV[8] = {
  0b00000001,
  0b00000010,
  0b00000100,
  0b00001000,
  0b00010000,
  0b00100000,
  0b01000000,
  0b10000000
  };

void setup()
{
  DDRD = B11111111;
}

byte text[] = {32, 144, 142, 127, 144, 135, 128, 141, 32, 134, 127, 32, 129, 140, 135, 139, 127, 140, 135, 132};

void print_text(byte text[]) {
    for (int i = 0; i < 20; i++) {
        print_symbol(alphabet[text[i] - 32], 5);
        delay(10);
    };
}

void print_symbol(byte symbol[], byte dim) {
    for (int i = 0; i < dim; i++) {
        PORTD = symbol[i];
        delay(10); 
    };
    PORTD = 0b00000000;
};

void loop()
{
    //print_text(text);
    print_symbol(POV, 8);
}
